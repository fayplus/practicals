# README #

This repository contains notebooks and other materials to 
support the practical work in COMP257 Data Science at Macquarie University, Sydney. 

Students should fork this repository into their own Bitbucket account. Then clone a 
working copy into your working environment and make changes to complete the tasks you are set. 
Finally you should open a pull request to submit your work to me.